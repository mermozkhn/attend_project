/**
 * 
 */
package model;

/**
 * Classe permet de gerer tout ce qui est objet Gestionnaire
 * @author Mermoz KANHONOU
 * @version 1.0
 * 
 * 
 */
public class Gestionnaire {
	int idg;
	String identifiant;
	String pasword;
	/**
	 * @param id
	 * @param identifiant
	 * @param password
	 */
	public Gestionnaire(int id, String identifiant, String password) {
		super();
		this.idg = id;
		this.identifiant = identifiant;
		this.pasword = password;
	}
	/**
	 * @return the id
	 */
	public int getIdg() {
		return idg;
	}
	/**
	 * @param id the id to set
	 */
	public void setIdg(int id) {
		this.idg = id;
	}
	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}
	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	/**
	 * @return the password
	 */
	public String getPasword() {
		return pasword;
	}
	/**
	 * @param password the password to set
	 */
	public void setPasword(String password) {
		this.pasword = password;
	}
}
