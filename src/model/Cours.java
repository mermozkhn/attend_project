/**
 * 
 */
package model;

/**
 * Classe permet de gerer tout ce qui est objet Cours
 * @author Mermoz KANHONOU
 * @version 1.0
 * 
 * 
 */
public class Cours {
	private int idCours;
	private String nom;
	private double masseHoraire;
	private String departement;
	public Cours (int id, String nom, Double masse, String depart) {
		this.idCours=id;
		this.nom=nom;
		this.masseHoraire=masse;
		this.departement=depart;
	}
	public Cours() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the idCours
	 */
	public int getIdCours() {
		return idCours;
	}
	/**
	 * @param idCours the idCours to set
	 */
	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the masseHoraire
	 */
	public double getMasseHoraire() {
		return masseHoraire;
	}
	/**
	 * @param masseHoraire the masseHoraire to set
	 */
	public void setMasseHoraire(double masseHoraire) {
		this.masseHoraire = masseHoraire;
	}
	/**
	 * @return the departement
	 */
	public String getDepartement() {
		return departement;
	}
	/**
	 * @param departement the departement to set
	 */
	public void setDepartement(String departement) {
		this.departement = departement;
	}

}
