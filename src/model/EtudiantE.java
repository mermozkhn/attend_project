/**
 * 
 */
package model;

/**
 * Classe permet de gerer tout ce qui est objet Etudiant
 * @author Mermoz KANHONOU
 * @version 1.0
 * 
 * 
 */
public class EtudiantE {
		int idEtudiant;
		String nom;
		String prenom ;
		String password ;
		String email ;
		String filiere ;
		int idgroupe;
		/**
		 * @param idEtudiant
		 * @param nom
		 * @param prenom
		 * @param motdepasse
		 * @param email
		 * @param filiere
		 * @param numtel
		 */
		public EtudiantE(int idEtudiant, String nom, String prenom, String motdepasse, String email, String filiere,
				int numtel) {
			this.idEtudiant = idEtudiant;
			this.nom = nom;
			this.prenom = prenom;
			this.password = motdepasse;
			this.email = email;
			this.filiere = filiere;
			this.idgroupe = numtel;
		}
		public EtudiantE() {
			// TODO Auto-generated constructor stub
		}
		/**
		 * @return the idEtudiant
		 */
		public int getIdEtudiant() {
			return idEtudiant;
		}
		/**
		 * @param idEtudiant the idEtudiant to set
		 */
		public void setIdEtudiant(int idEtudiant) {
			this.idEtudiant = idEtudiant;
		}
		/**
		 * @return the nom
		 */
		public String getNom() {
			return nom;
		}
		/**
		 * @param nom the nom to set
		 */
		public void setNom(String nom) {
			this.nom = nom;
		}
		/**
		 * @return the prenom
		 */
		public String getPrenom() {
			return prenom;
		}
		/**
		 * @param prenom the prenom to set
		 */
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		/**
		 * @return the motdepasse
		 */
		public String getMotdepasse() {
			return password;
		}
		/**
		 * @param motdepasse the motdepasse to set
		 */
		public void setMotdepasse(String motdepasse) {
			this.password = motdepasse;
		}
		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}
		/**
		 * @param email the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}
		/**
		 * @return the filiere
		 */
		public String getFiliere() {
			return filiere;
		}
		/**
		 * @param filiere the filiere to set
		 */
		public void setFiliere(String filiere) {
			this.filiere = filiere;
		}
		/**
		 * @return the numtel
		 */
		public int getIdGroupe() {
			return idgroupe;
		}
		/**
		 * @param numtel the numtel to set
		 */
		public void setIdGroupe(int numtel) {
			this.idgroupe = numtel;
		}

}
