package model;

/**
 * Classe permet de gerer tout ce qui est objet Enseignant
 * @author Mermoz KANHONOU
 * @version 1.0
 * 
 * 
 */
public class Enseignant {
	/**
	 * 
	 */
	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}
	int id;
	String nom;
	String prenom ;
	String password ;
	String email ;
	int numtel;
	String bureau ;
	/**
	 * @param id
	 * @param nom
	 * @param prenom
	 * @param password
	 * @param email
	 * @param numtel
	 * @param bureau
	 * 
	 */
	public Enseignant(int id, String nom, String prenom, String password, String email, int numtel, String bureau) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.email = email;
		this.numtel = numtel;
		this.bureau = bureau;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the bureau
	 */
	public String getBureau() {
		return bureau;
	}
	/**
	 * @param bureau the bureau to set
	 */
	public void setBureau(String bureau) {
		this.bureau = bureau;
	}
	/**
	 * @return the numtel
	 */
	public int getNumtel() {
		return numtel;
	}
	/**
	 * @param numtel the numtel to set
	 */
	public void setNumtel(int numtel) {
		this.numtel = numtel;
	}
	
	
}
