/**
 * 
 * @author Kevin ADJIWANOU
 * @version 1.1
 */
package GUI;
import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTable;
import java.awt.Font;
import DAO.AbsenceDAO;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/*
 *  Fenetre pour voir le planing
 */
public class AbsenceEGUI extends JFrame {

	/*
	 * Pour les éléments de la fenetre
	 */
	private JPanel contentPane;
	/*
	 * Parametre de tableau
	 */
	private JTable table;
	/*
	 * Parametre de tableau
	 */
	private JTable table_1;
	/*
	 * Parametre de tableau
	 */
	private JTable table_2;
	/*
	 * Parametre de tableau
	 */
	private JTable table_3;
	/*
	 * Parametre de tableau
	 */
	private JTable table_4;
	/*
	 * Parametre de tableau
	 */
	private JTable table_5;
	/*
	 * Parametre de tableau
	 */
	private JTable table_6;
	/*
	 * Parametre de tableau
	 */
	private JTable table_7;
	/*
	 * Parametre de tableau
	 */
	private JTable table_8;
	/*
	 * Variable permettant de mettre un scrolbar
	 */
	private JScrollPane scrollPane;
	/*
	 * JLabel pour gerer laa fenetre
	 */
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application
	 * @param args pour 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AbsenceEGUI frame = new AbsenceEGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AbsenceEGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 899, 557);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("PLANNING");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(365, 10, 145, 57);
		contentPane.add(lblNewLabel_1);
		
		table = new JTable();
		table.setBounds(97, 138, 130, -71);
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setBounds(64, 192, 251, -137);
		contentPane.add(table_1);
		
		table_2 = new JTable();
		table_2.setBounds(128, 148, 1, 1);
		contentPane.add(table_2);
		
		table_3 = new JTable();
		table_3.setBounds(107, 84, 1, 1);
		contentPane.add(table_3);
		
		table_4 = new JTable();
		table_4.setBounds(280, 138, -182, -72);
		contentPane.add(table_4);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(82, 111, 709, 390);
		contentPane.add(scrollPane);
		
		table_5 = new JTable();
		scrollPane.setViewportView(table_5);
		
		
		table_6 = new JTable();
		table_6.setBounds(206, 148, 1, 1);
		contentPane.add(table_6);
		
		table_7 = new JTable();
		table_7.setBounds(249, 138, 1, 1);
		contentPane.add(table_7);
		
		table_8 = new JTable();
		table_8.setBounds(226, 138, 1, 1);
		contentPane.add(table_8);
		
		AbsenceDAO k= new AbsenceDAO();
		k.idp(1,table_5);
		
		lblNewLabel_2 = new JLabel("Aujourd'hui");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(107, 85, 70, 13);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("RETOUR");
		btnNewButton.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MenuGUI i= new MenuGUI();
				i.setVisible(true);
			}
		});
		btnNewButton.setBounds(694, 43, 130, 42);
		contentPane.add(btnNewButton);
		
	}
}