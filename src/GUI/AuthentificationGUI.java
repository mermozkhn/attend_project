package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import DAO.AuthentificationDAO ;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;


/**
 * @author Mermoz KANHONOU
 *
 */
public class AuthentificationGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField id;
	/**
	 * 
	 */
	private JPasswordField motpass;

	/**
	 * Launch the application. 
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AuthentificationGUI frame = new AuthentificationGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public AuthentificationGUI() {
		setFont(new Font("Arial Black", Font.PLAIN, 11));
		setTitle("Authentification");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 959, 538);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Mermoz KANHONOU\\Downloads\\esigelec.png"));
		lblNewLabel.setBounds(289, 11, 478, 107);
		contentPane.add(lblNewLabel);
		
		id = new JTextField();
		id.setText("Entrer identifiant");
		id.setBounds(493, 184, 150, 27);
		contentPane.add(id);
		id.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Identifiant");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(298, 179, 81, 33);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Mots de passe");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(298, 272, 100, 33);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Connexion");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AuthentificationDAO auth = new AuthentificationDAO();
				if ((auth.Verification(id.getText(), motpass.getText()))==1) {
				setVisible(false);
				AccueilGUI a = new AccueilGUI();
				a.setVisible(true);
				}
			}
		});
		btnNewButton.setBounds(449, 340, 100, 23);
		contentPane.add(btnNewButton);
		
		motpass = new JPasswordField();
		motpass.setBounds(493, 280, 150, 25);
		contentPane.add(motpass);
	}
}
