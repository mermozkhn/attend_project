package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Mermoz KANHONOU
 *
 */
public class EtudiantMenuGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EtudiantMenuGUI frame = new EtudiantMenuGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public EtudiantMenuGUI() {
		setTitle("EtudiantMenu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 813, 514);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton retourbutton = new JButton("Retour");
		retourbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AccueilGUI i = new AccueilGUI();
				i.setVisible(true);
			}
		});
		retourbutton.setBounds(656, 40, 89, 29);
		contentPane.add(retourbutton);
		
		JButton modifierbutton = new JButton("Modifier");
		modifierbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateEtudiantGUI a = new UpdateEtudiantGUI();
				a.setVisible(true);
			}
		});
		modifierbutton.setBounds(161, 207, 89, 44);
		contentPane.add(modifierbutton);
		
		JButton ajouterbutton = new JButton("Ajouter");
		ajouterbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				EtudiantEGUI g = new EtudiantEGUI();
				g.setVisible(true);
			}
		});
		ajouterbutton.setBounds(161, 95, 89, 44);
		contentPane.add(ajouterbutton);
		
		JButton supprimerbutton = new JButton("Supprimer");
		supprimerbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateEtudiantGUI a = new UpdateEtudiantGUI();
				a.setVisible(true);
			}
		});
		supprimerbutton.setBounds(161, 331, 89, 44);
		contentPane.add(supprimerbutton);
	}

}
