package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import model.Enseignant;
import DAO.EnseignantDAO;

/**
 * @author Mermoz KANHONOU
 *
 */
public class AjoutEnseignantGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField idT;
	/**
	 * 
	 */
	private JTextField nomT;
	/**
	 * 
	 */
	private JTextField prenomT;
	/**
	 * 
	 */
	private JTextField motdepasseT;
	/**
	 * 
	 */
	private JTextField groupeT;
	/**
	 * 
	 */
	private JTextField bureau;
	/**
	 * 
	 */
	private JTextField filiereT;
	/**
	 * 
	 */
	private JTextField emailT;
	/**
	 * 
	 */
	private JButton btnNewButton_1;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AjoutEnseignantGUI frame = new AjoutEnseignantGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public AjoutEnseignantGUI() {
		setTitle("Ajout Enseignant");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1069, 541);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		idT = new JTextField();
		idT.setText("Id ");
		idT.setBounds(150, 49, 127, 20);
		contentPane.add(idT);
		idT.setColumns(10);
		
		nomT = new JTextField();
		nomT.setText("nom");
		nomT.setBounds(150, 124, 127, 20);
		contentPane.add(nomT);
		nomT.setColumns(10);
		
		prenomT = new JTextField();
		prenomT.setText("prenom");
		prenomT.setBounds(150, 211, 127, 20);
		contentPane.add(prenomT);
		prenomT.setColumns(10);
		
		motdepasseT = new JTextField();
		motdepasseT.setText("mot de passe");
		motdepasseT.setBounds(571, 49, 133, 20);
		contentPane.add(motdepasseT);
		motdepasseT.setColumns(10);
		
		bureau = new JTextField();
		bureau.setText("Bureau");
		bureau.setBounds(577, 293, 127, 20);
		contentPane.add(bureau);
		bureau.setColumns(10);
		
		filiereT = new JTextField();
		filiereT.setText("Numero Tel");
		filiereT.setBounds(577, 164, 127, 20);
		contentPane.add(filiereT);
		filiereT.setColumns(10);
		
		emailT = new JTextField();
		emailT.setText("email");
		emailT.setBounds(150, 316, 127, 20);
		contentPane.add(emailT);
		emailT.setColumns(10);
		
		JButton btnNewButton = new JButton("RETOUR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				EnseignantEGUI i = new EnseignantEGUI();
				i.setVisible(true);
			}
		});
		btnNewButton.setBounds(917, 73, 89, 23);
		contentPane.add(btnNewButton);
		btnNewButton_1 = new JButton("VALIDER");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Enseignant n = new Enseignant(Integer.parseInt(idT.getText()),nomT.getText(),prenomT.getText(),motdepasseT.getText(),emailT.getText(),Integer.parseInt(filiereT.getText()),bureau.getText());
				EnseignantDAO o = new EnseignantDAO();
				o.add(n);
				JOptionPane.showMessageDialog(null, "Ajout de enseignant reussi");
				setVisible(false);
				AccueilGUI i = new AccueilGUI();
				i.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(398, 434, 89, 23);
		contentPane.add(btnNewButton_1);
	}

}
