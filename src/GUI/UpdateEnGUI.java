package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import model.Enseignant;
import DAO.EnseignantDAO;
import DAO.EtudiantDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Mermoz KANHONOU
 *
 */
public class UpdateEnGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField text1;
	/**
	 * 
	 */
	private JTextField txtPrenom;
	/**
	 * 
	 */
	private JTextField txtPassword;
	/**
	 * 
	 */
	private JTextField txtEmail;
	/**
	 * 
	 */
	private JTextField txtFiliere;
	/**
	 * 
	 */
	private JTextField txtFiliere1;
	/**
	 * 
	 */
	private JButton modifButton;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Enseignant y = new Enseignant ();
					UpdateEnGUI frame = new UpdateEnGUI(y);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * @param y
	 */
	public UpdateEnGUI(Enseignant y) {
		setTitle("Update Enseignant");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 919, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
	    
		
		text1 = new JTextField();
		text1.setText(y.getNom());
		text1.setBounds(82, 53, 108, 32);
		contentPane.add(text1);
		text1.setColumns(10);
		
		txtPrenom = new JTextField();
		txtPrenom.setText(y.getPrenom());
		txtPrenom.setBounds(82, 149, 108, 32);
		contentPane.add(txtPrenom);
		txtPrenom.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setText(y.getPassword());
		txtPassword.setBounds(82, 278, 102, 32);
		contentPane.add(txtPassword);
		txtPassword.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setText(y.getEmail());
		txtEmail.setBounds(606, 53, 214, 32);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtFiliere = new JTextField();
		txtFiliere.setText(Integer.toString(y.getNumtel()));
		txtFiliere.setBounds(647, 149, 102, 32);
		contentPane.add(txtFiliere);
		txtFiliere.setColumns(10);
		
		txtFiliere1 = new JTextField();
		txtFiliere1.setText(y.getBureau());
		txtFiliere1.setBounds(617, 263, 227, 32);
		contentPane.add(txtFiliere1);
		txtFiliere1.setColumns(10);
		
		modifButton = new JButton("Modifier");
		modifButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				y.setNom(text1.getText());
				y.setPrenom(txtPrenom.getText());
				y.setPassword(txtPassword.getText());
				y.setEmail(txtEmail.getText());
				y.setNumtel(Integer.parseInt(txtFiliere.getText()));
				y.setBureau(txtFiliere1.getText());
				EnseignantDAO e1 = new EnseignantDAO();
				e1.update(y);
				JOptionPane.showMessageDialog(null, " Modification réussite avec perfection Merci !!!");
				setVisible(false);
				AccueilGUI u = new AccueilGUI();
				u.setVisible(true);
				
			}
		});
		modifButton.setBounds(424, 365, 89, 23);
		contentPane.add(modifButton);

	}

}
