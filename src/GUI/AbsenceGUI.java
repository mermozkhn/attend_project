/**
 * 
 * @author Kevin ADJIWANOU
 */
package GUI;

import java.awt.EventQueue;
import DAO.AbsenceDAO;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/*
 * Classe qui hérite de JFrame pour la fenetre
 */
public class AbsenceGUI extends JFrame {

	/*
	 * Variable pour gerer la fenetre
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 * @param args pour le main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AbsenceGUI frame = new AbsenceGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AbsenceGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 788, 445);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(80, 134, 560, 228);
		contentPane.add(scrollPane);
		
		JTable table = new JTable();
		scrollPane.setViewportView(table);
		AbsenceDAO v = new AbsenceDAO();
		v.abs("1234",table);
		
		JLabel lblNewLabel = new JLabel("ABSENCE");
		lblNewLabel.setFont(new Font("Thorndale AMT", Font.PLAIN, 14));
		lblNewLabel.setBounds(323, 43, 108, 39);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Retour");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AccueilGUI a = new AccueilGUI();
				a.setVisible(true);
			}
		});
		btnNewButton.setBounds(675, 28, 89, 23);
		contentPane.add(btnNewButton);
	}
}
