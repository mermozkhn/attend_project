package GUI;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * @author Mermoz KANHONOU
 *
 */
public class CoursEGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CoursEGUI frame = new CoursEGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public CoursEGUI() {
		setTitle("COURS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 898, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton btnNewButton = new JButton("RETOUR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AccueilGUI i = new AccueilGUI();
				i.setVisible(true);
			}
		});
		btnNewButton.setBounds(750, 24, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton addbutton = new JButton("Ajouter");
		addbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AjoutCoursGUI u = new AjoutCoursGUI();
				u.setVisible(true);
			}
		});
		addbutton.setBounds(149, 68, 89, 39);
		contentPane.add(addbutton);
		
		JButton modifierbutton = new JButton("Modifier");
		modifierbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateCoursGUI p = new UpdateCoursGUI();
				p.setVisible(true);
			}
		});
		modifierbutton.setBounds(149, 173, 89, 39);
		contentPane.add(modifierbutton);
		
		JButton supprimerbutton = new JButton("Supprimer");
		supprimerbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateCoursGUI p = new UpdateCoursGUI();
				p.setVisible(true);
				
			}
		});
		supprimerbutton.setBounds(149, 298, 89, 39);
		contentPane.add(supprimerbutton);
	}
}
