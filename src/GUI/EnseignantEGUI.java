package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Mermoz KANHONOU
 *
 */
public class EnseignantEGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnseignantEGUI frame = new EnseignantEGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public EnseignantEGUI() {
		setTitle("Gestion Enseignant");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 802, 499);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("RETOUR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AccueilGUI i = new AccueilGUI();
				i.setVisible(true);
			}
		});
		btnNewButton.setBounds(646, 43, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton ajouterbutton = new JButton("Ajouter");
		ajouterbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				AjoutEnseignantGUI n = new AjoutEnseignantGUI();
				n.setVisible(true);
			}
		});
		ajouterbutton.setBounds(160, 71, 89, 39);
		contentPane.add(ajouterbutton);
		
		JButton supprimerbutton = new JButton("Supprimer");
		supprimerbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateEnseignantGUI n = new UpdateEnseignantGUI();
				n.setVisible(true);
			}
		});
		supprimerbutton.setBounds(160, 348, 89, 39);
		contentPane.add(supprimerbutton);
		
		JButton modifierbutton = new JButton("Modifier");
		modifierbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UpdateEnseignantGUI n = new UpdateEnseignantGUI();
				n.setVisible(true);
			}
		});
		modifierbutton.setBounds(160, 208, 89, 39);
		contentPane.add(modifierbutton);
	}

}
