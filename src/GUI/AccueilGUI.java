package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import GUI.EnseignantEGUI;
import GUI.AbsenceEGUI;
import GUI.CoursEGUI;
import GUI.EtudiantEGUI;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/*
 * Classe qui herite de JFRame pour la fenetre
 */
public class AccueilGUI extends JFrame {

	/*
	 * Variable contentPane pour les element de la fenetre
	 */
	private JPanel contentPane;
	/*
	 * Vriable pour ecrire sur la fenetre
	 */
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 * @param args pour le main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccueilGUI frame = new AccueilGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccueilGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1061, 529);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNewLabel = new JLabel("  MENU");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewLabel.setBounds(448, 24, 89, 33);
		contentPane.add(lblNewLabel);
		
		JButton btn1 = new JButton("Etudiant");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				EtudiantMenuGUI g = new EtudiantMenuGUI();
				g.setVisible(true);
			}
		});
		btn1.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		btn1.setForeground(new Color(255, 0, 0));
		btn1.setBounds(106, 124, 103, 46);
		contentPane.add(btn1);
		
		JButton btnNewButton_1 = new JButton("Enseignant");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				EnseignantEGUI E = new EnseignantEGUI();
				E.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		btnNewButton_1.setForeground(new Color(255, 0, 0));
		btnNewButton_1.setBounds(106, 343, 103, 46);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Cours");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				CoursEGUI f = new CoursEGUI();
				f.setVisible(true);
			}
		});
		btnNewButton_2.setForeground(new Color(255, 0, 0));
		btnNewButton_2.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		btnNewButton_2.setBounds(762, 124, 89, 46);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Absence");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MenuGUI a = new MenuGUI();
				a.setVisible(true);
			}
		});
		btnNewButton_3.setForeground(new Color(255, 0, 0));
		btnNewButton_3.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		btnNewButton_3.setBounds(762, 343, 89, 46);
		contentPane.add(btnNewButton_3);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Mermoz KANHONOU\\Pictures\\esig.jpg"));
		lblNewLabel_1.setBounds(10, 11, 1027, 470);
		contentPane.add(lblNewLabel_1);
	}
}
