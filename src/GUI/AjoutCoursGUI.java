package GUI;

	import java.awt.EventQueue;

	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import javax.swing.border.EmptyBorder;
	import javax.swing.JList;
	import javax.swing.JOptionPane;
	import javax.swing.JTree;
	import javax.swing.JTextField;
	import javax.swing.JButton;
	import java.awt.event.ActionListener;
	import java.awt.event.ActionEvent;
	import model.Cours;
	import DAO.CoursDAO;
import javax.swing.JLabel;

/*
 * Classe qui herite de JFrame pour la fenetre
 */
public class AjoutCoursGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField idCours;
	/**
	 * 
	 */
	private JTextField nomC;
	/**
	 * 
	 */
	private JTextField masseHoraire;
	/**
	 * 
	 */
	private JTextField departement;
	/**
	 * 
	 */
	private JButton btnNewButton_1;

	/**
	 * Launch the application.
	 *
	 * @param args pour ke main
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AjoutCoursGUI frame = new AjoutCoursGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
		/**
		 * Create the frame.
		 */
		public AjoutCoursGUI() {
			setTitle("Ajout Cours");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 1069, 541);
			contentPane = new JPanel();
			contentPane.setToolTipText("");
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			idCours = new JTextField();
			idCours.setText("IdCours ");
			idCours.setBounds(278, 74, 127, 20);
			contentPane.add(idCours);
			idCours.setColumns(10);
			
			nomC = new JTextField();
			nomC.setText("Nom");
			nomC.setBounds(278, 202, 127, 20);
			contentPane.add(nomC);
			nomC.setColumns(10);
			
			masseHoraire = new JTextField();
			masseHoraire.setText("Masse horaire");
			masseHoraire.setBounds(278, 304, 127, 20);
			contentPane.add(masseHoraire);
			masseHoraire.setColumns(10);
			
			departement = new JTextField();
			departement.setText("Departement");
			departement.setBounds(720, 202, 133, 20);
			contentPane.add(departement);
			departement.setColumns(10);
			
			JButton btnNewButton = new JButton("RETOUR");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					EtudiantMenuGUI i = new EtudiantMenuGUI();
					i.setVisible(true);
				}
			});
			btnNewButton.setBounds(917, 73, 89, 23);
			contentPane.add(btnNewButton);
			btnNewButton_1 = new JButton("VALIDER");
			btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Cours c1 = new Cours(Integer.parseInt(idCours.getText()),nomC.getText(),Double.parseDouble(masseHoraire.getText()),departement.getText());
					CoursDAO c = new CoursDAO();
					c.add(c1);
					JOptionPane.showMessageDialog(null, "Ajout de Cours reussi");
					setVisible(false);
					AccueilGUI i = new AccueilGUI();
					i.setVisible(true);
				}
			});
			btnNewButton_1.setBounds(398, 434, 89, 23);
			contentPane.add(btnNewButton_1);
			
			JLabel lblNewLabel = new JLabel("IdCours");
			lblNewLabel.setBounds(106, 77, 49, 14);
			contentPane.add(lblNewLabel);
			
			JLabel lblNewLabel_1 = new JLabel("Intitule du Cours");
			lblNewLabel_1.setBounds(106, 205, 115, 14);
			contentPane.add(lblNewLabel_1);
			
			JLabel lblNewLabel_2 = new JLabel("Masse horaire ( h )");
			lblNewLabel_2.setBounds(106, 307, 115, 14);
			contentPane.add(lblNewLabel_2);
			
			JLabel lblNewLabel_3 = new JLabel("Departement");
			lblNewLabel_3.setBounds(602, 205, 82, 14);
			contentPane.add(lblNewLabel_3);
		}
	}


