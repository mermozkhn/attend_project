package GUI;

		import java.awt.EventQueue;
		import java.sql.ResultSet;
		import java.sql.ResultSetMetaData;
		import java.sql.SQLException;
		import java.util.Vector;

		import javax.swing.JFrame;
		import javax.swing.JOptionPane;
		import javax.swing.JPanel;
		import javax.swing.JScrollPane;
		import javax.swing.JTable;
		import javax.swing.border.EmptyBorder;
		import javax.swing.table.DefaultTableModel;
		import javax.swing.table.TableModel;

		import DAO.CoursDAO;
		import javax.swing.ListSelectionModel;
		import javax.swing.JButton;
		import javax.swing.JTextField;
		import java.awt.event.ActionListener;
		import java.awt.event.ActionEvent;
		import javax.swing.JLabel;
		import java.awt.Font;
		import java.awt.Color;

/**
 * @author Mermoz KANHONOU
 *
 */
public class UpdateCoursGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateCoursGUI frame = new UpdateCoursGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public UpdateCoursGUI() {
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setBounds(100, 100, 1024, 510);
				getContentPane().setLayout(null);
				ResultSet dz = null ;
				CoursDAO c = new CoursDAO();
				dz=c.getAll();
				
				try {
					JTable table =new JTable(buildTableModel(dz));
					table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					table.setCellSelectionEnabled(true);
					table.setColumnSelectionAllowed(true);
					JScrollPane pane= new JScrollPane(table);
					pane.setBounds(10,10,800,500);
					getContentPane().add(pane);
					
					JButton modifierButton = new JButton("Modifier");
					modifierButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							SelectedIdCoursGUI u1 =  new SelectedIdCoursGUI();
							u1.setVisible(true);
						}
					});
					modifierButton.setBounds(870, 231, 89, 23);
					getContentPane().add(modifierButton);
					
					JButton deleteButton = new JButton("Supprimer");
					deleteButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							DeleteCoursGUI d1 = new DeleteCoursGUI();
							d1.setVisible(true);
						}
					});
					deleteButton.setBounds(870, 309, 89, 23);
					getContentPane().add(deleteButton);
					
					JButton btnNewButton = new JButton("Retour");
					btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							setVisible(false);
							CoursEGUI g = new CoursEGUI();
							g.setVisible(true);
						}
					});
					btnNewButton.setBounds(870, 146, 89, 23);
					getContentPane().add(btnNewButton);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			/**
			 * @param rs
			 * @return
			 * @throws SQLException
			 */
			public static DefaultTableModel buildTableModel(ResultSet rs)
			        throws SQLException {

			    ResultSetMetaData metaData = rs.getMetaData();

			    // names of columns
			    Vector<String> columnNames = new Vector<String>();
			    int columnCount = metaData.getColumnCount();
			    for (int column = 1; column <= columnCount; column++) {
			        columnNames.add(metaData.getColumnName(column));
			    }

			    // data of the table
			    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
			    while (rs.next()) {
			        Vector<Object> vector = new Vector<Object>();
			        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
			            vector.add(rs.getObject(columnIndex));
			        }
			        data.add(vector);
			    }

			    return new DefaultTableModel(data, columnNames);

			}
		

	}
