package GUI;

		import java.awt.EventQueue;
		import DAO.CoursDAO;

		import javax.swing.JFrame;
		import javax.swing.JPanel;
		import javax.swing.border.EmptyBorder;
		import javax.swing.JLabel;
		import javax.swing.JOptionPane;

		import java.awt.Font;
		import javax.swing.JTextField;
		import javax.swing.JButton;
		import java.awt.event.ActionListener;
		import java.awt.event.ActionEvent;

/**
 * @author Mermoz KANHONOU
 *
 */
public class DeleteCoursGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField idDelete;
	/**
	 * 
	 */
	private JButton supprimerButton;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteCoursGUI frame = new DeleteCoursGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public DeleteCoursGUI() {
				setTitle("Delete Cours");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setBounds(100, 100, 612, 405);
				contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

				setContentPane(contentPane);
				contentPane.setLayout(null);
				
				JLabel lblNewLabel = new JLabel("                    Veuillez rentrer l ' id du Cours à supprimer ");
				lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
				lblNewLabel.setBounds(87, 28, 372, 30);
				contentPane.add(lblNewLabel);
				
				idDelete = new JTextField();
				idDelete.setText("Id");
				idDelete.setBounds(241, 98, 96, 20);
				contentPane.add(idDelete);
				idDelete.setColumns(10);
				
				supprimerButton = new JButton("Supprimer");
				supprimerButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int j,k;
						j = Integer.parseInt(idDelete.getText());
						CoursDAO d = new CoursDAO();
						k = d.delete(j);
						if (k != 0 ) {
							JOptionPane.showMessageDialog(null, "Etudiant supprime avec succes !!!");
							setVisible(false);
							UpdateCoursGUI a = new UpdateCoursGUI();
						    a.setVisible(true);
						}
						else {
							JOptionPane.showMessageDialog(null,"Aucune ligne n'a ete supprime ");
						}
					}
				});
				supprimerButton.setBounds(241, 177, 89, 23);
				contentPane.add(supprimerButton);
			}

}
