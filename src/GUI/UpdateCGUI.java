package GUI;

		import java.awt.EventQueue;

		import javax.swing.JFrame;
		import javax.swing.JOptionPane;
		import javax.swing.JPanel;
		import javax.swing.border.EmptyBorder;
		import javax.swing.JTextField;
		import model.Cours;
		import DAO.CoursDAO;
		import javax.swing.JButton;
		import java.awt.event.ActionListener;
		import java.awt.event.ActionEvent;

/**
 * @author Mermoz KANHONOU
 *
 */
public class UpdateCGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField text2;
	/**
	 * 
	 */
	private JTextField txtPrenom1;
	/**
	 * 
	 */
	private JTextField txtPassword1;
	/**
	 * 
	 */
	private JButton modifButton1;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cours y = new Cours();
					UpdateCGUI frame = new UpdateCGUI(y);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the frame.
	 */
	/**
	 * @param y
	 */
	public UpdateCGUI(Cours y) {
				setTitle("Update Cours");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setBounds(100, 100, 919, 500);
				contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

				setContentPane(contentPane);
				contentPane.setLayout(null);
			    
				
				text2 = new JTextField();
				text2.setText(y.getNom());
				text2.setBounds(125, 76, 108, 32);
				contentPane.add(text2);
				text2.setColumns(10);
				
				txtPrenom1 = new JTextField();
				txtPrenom1.setText(Double.toString(y.getMasseHoraire()));
				txtPrenom1.setBounds(125, 281, 108, 32);
				contentPane.add(txtPrenom1);
				txtPrenom1.setColumns(10);
				
				txtPassword1 = new JTextField();
				txtPassword1.setText(y.getDepartement());
				txtPassword1.setBounds(560, 149, 219, 32);
				contentPane.add(txtPassword1);
				txtPassword1.setColumns(10);
				
				modifButton1 = new JButton("Modifier");
				modifButton1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						y.setNom(text2.getText());
						y.setMasseHoraire(Double.parseDouble(txtPrenom1.getText()));
						y.setDepartement(txtPassword1.getText());
						CoursDAO e1 = new CoursDAO();
						e1.update(y);
						JOptionPane.showMessageDialog(null, " Modification réussite avec perfection Merci !!!");
						setVisible(false);
						AccueilGUI u = new AccueilGUI();
						u.setVisible(true);
						
					}
				});
				modifButton1.setBounds(514, 358, 89, 23);
				contentPane.add(modifButton1);
			}

}
