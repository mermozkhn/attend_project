package GUI;

		import DAO.EnseignantDAO;
		import model.Enseignant;
		import java.awt.EventQueue;

		import javax.swing.JFrame;
		import javax.swing.JPanel;
		import javax.swing.border.EmptyBorder;
		import javax.swing.JLabel;
		import java.awt.Font;
		import javax.swing.JTextField;
		import javax.swing.JButton;
		import java.awt.event.ActionListener;
		import java.awt.event.ActionEvent;
/**
 * @author Mermoz KANHONOU
 *
 */
public class SelectedEnseignantGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	/**
	 * 
	 */
	private JTextField idTex;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectedEnseignantGUI frame = new SelectedEnseignantGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/**
	 * 
	 */
	public SelectedEnseignantGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 527, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Veuillez choisir l'id de l'element à modifier");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel.setBounds(110, 29, 285, 36);
		contentPane.add(lblNewLabel);
		
		idTex = new JTextField();
		idTex.setText("Id");
		idTex.setBounds(211, 124, 96, 20);
		contentPane.add(idTex);
		idTex.setColumns(10);
		
		JButton validate = new JButton("Valider");
		validate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				int i;
				EnseignantDAO b = new EnseignantDAO();
				i = Integer.parseInt(idTex.getText());
				Enseignant t1 = b.get(i);
				UpdateEnGUI p1= new UpdateEnGUI(t1);
				p1.setVisible(true);
				
			}
		});
		validate.setBounds(196, 219, 89, 23);
		contentPane.add(validate);
	}

}
