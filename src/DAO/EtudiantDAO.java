package DAO;

import model.EtudiantE;
import java.sql.*;
import java.util.ArrayList;


/**
 *  Classe permettant de gérer tout ce qui a rapport avec la database de Etudiant
 * @author Mermoz KANHONOU
 * @version 1.0
 *
 */
public class EtudiantDAO extends ConnectionDAO {

	/**
	 * Classe d'acces aux donnees contenues dans la table supplier
	 * 
	 * @author Mermoz KANHONOU
	 * @version 1.0
	 * */
		public EtudiantDAO() {
			super();
		}

		/**
		 * Permet d'ajouter un Etudiant dans la table Etudiant.
		 * Le mode est auto-commit par defaut : chaque insertion est validee
		 * 
		 * @param enseignant l'enseignant a ajouter
		 * @return retourne le nombre de lignes ajoutees dans la table
		 */
		public int add(EtudiantE enseignant) {
			Connection con = null;
			PreparedStatement ps = null;
			int returnValue = 0;

			// connexion a la base de donnees
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				// preparation de l'instruction SQL, chaque ? represente une valeur
				// a communiquer dans l'insertion.
				// les getters permettent de recuperer les valeurs des attributs souhaites
				ps = con.prepareStatement("INSERT INTO Etudiant(idEtudiant, nom, prenom, password, email, filiere, idGroupe) VALUES(?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, enseignant.getIdEtudiant());
				ps.setString(2, enseignant.getNom());
				ps.setString(3, enseignant.getPrenom());
				ps.setString(4, enseignant.getMotdepasse());
				ps.setString(5, enseignant.getEmail());
				ps.setString(6, enseignant.getFiliere());
				ps.setInt(7, enseignant.getIdGroupe());

				// Execution de la requete
				returnValue = ps.executeUpdate();

			} catch (Exception e) {
				if (e.getMessage().contains("ORA-00001"))
					System.out.println("Cet identifiant de enseignant existe déjà. Ajout impossible !");
				else
					e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return returnValue;
		}

		/**
		 * Permet de modifier un fournisseur dans la table supplier.
		 * Le mode est auto-commit par defaut : chaque modification est validee
		 * 
		 * @param supplier le fournisseur a modifier
		 * @return retourne le nombre de lignes modifiees dans la table
		 */
		public int update(EtudiantE enseignant) {
			Connection con = null;
			PreparedStatement ps = null;
			int returnValue = 0;

			// connexion a la base de donnees
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				// preparation de l'instruction SQL, chaque ? represente une valeur
				// a communiquer dans la modification.
				// les getters permettent de recuperer les valeurs des attributs souhaites
				ps = con.prepareStatement("UPDATE Etudiant set nom = ?, prenom = ?, password = ?, email = ?, filiere = ?, idGroupe = ? WHERE idEtudiant = ?");
				ps.setString(1, enseignant.getNom());
				ps.setString(2, enseignant.getPrenom());
				ps.setString(3, enseignant.getMotdepasse());
				ps.setString(4, enseignant.getEmail());
				ps.setString(5, enseignant.getFiliere());
				ps.setInt(6, enseignant.getIdGroupe());
				ps.setInt(7, enseignant.getIdEtudiant());

				// Execution de la requete
				returnValue = ps.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return returnValue;
		}

		/**
		 * Permet de supprimer un etudiant par id dans la table etudiant.
		 * Si ce dernier possede des articles, la suppression n'a pas lieu.
		 * Le mode est auto-commit par defaut : chaque suppression est validee
		 * 
		 * @param id l'id de etudiant à supprimer
		 * @return retourne le nombre de lignes supprimees dans la table
		 */
		public int delete(int id) {
			Connection con = null;
			PreparedStatement ps = null;
			int returnValue = 0;

			// connexion a la base de donnees
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				// preparation de l'instruction SQL, le ? represente la valeur de l'ID
				// a communiquer dans la suppression.
				// le getter permet de recuperer la valeur de l'ID du etudiant
				ps = con.prepareStatement("DELETE  FROM Etudiant WHERE idEtudiant = ?");
				ps.setInt(1, id);
				// Execution de la requete
				returnValue = ps.executeUpdate();

			} catch (Exception e) {
				if (e.getMessage().contains("ORA-02292"))
					System.out.println("Ce fournisseur possede des articles, suppression impossible !"
							         + " Supprimer d'abord ses articles ou utiiser la méthode de suppression avec articles.");
				else
					e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return returnValue;
		}


		/**
		 * Permet de recuperer un enseignant a partir de sa reference
		 * 
		 * @param reference la reference de enseigannt a recuperer
		 * @return le fournisseur trouve;
		 * 			null si aucun fournisseur ne correspond a cette reference
		 */
		public EtudiantE get(int id) {
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			EtudiantE returnValue = null;

			// connexion a la base de donnees
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Etudiant WHERE idEtudiant = ?");
				ps.setInt(1, id);

				// on execute la requete
				// rs contient un pointeur situe juste avant la premiere ligne retournee
				rs = ps.executeQuery();
				// passe a la premiere (et unique) ligne retournee
				if (rs.next()) {
					returnValue = new EtudiantE(rs.getInt("idEtudiant"),
										       rs.getString("nom"),
										       rs.getString("prenom"),
										       rs.getString("password"),
										       rs.getString("email"),
										       rs.getString("filiere"),
										       rs.getInt("idGroupe"));
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du ResultSet, du PreparedStatement et de la Connexion
				try {
					if (rs != null) {
						rs.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (ps != null) {
						ps.close();
					}
				} catch (Exception ignore) {
				}
				try {
					if (con != null) {
						con.close();
					}
				} catch (Exception ignore) {
				}
			}
			return returnValue;
		}

	
		public ResultSet getAll() {
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM Etudiant");

				// on execute la requete
				// rs contient un pointeur situe juste avant la premiere ligne retournee
				rs = ps.executeQuery();
			}
			catch (Exception ee) {
					ee.printStackTrace();
			
			
		}
			return rs;
	}
		public ResultSet countAll() {
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT COUNT (*) FROM Etudiant");

				// on execute la requete
				// rs contient un pointeur situe juste avant la premiere ligne retournee
				rs = ps.executeQuery();
			}
			catch (Exception ee) {
					ee.printStackTrace();
			
			
		}
			return rs;
	}
}
// ORA-01438 : pour une erreur à une case pour pas avoir remplir corrcetement la case 
