package DAO;
import java.sql.*;
import java.util.ArrayList;
import model.Enseignant ;

/**
 * Classe d'acces aux donnees contenues dans la database de Enseignant
 * 
 * @author Mermoz KANHONOU
 * @version 1.0
 * */
public class EnseignantDAO extends ConnectionDAO {
	/**
	 * Constructor
	 * 
	 */
	public EnseignantDAO() {
		super();
	}

	/**
	 * Permet d'ajouter un enseignant dans la table enseignant.
	 * Le mode est auto-commit par defaut : chaque insertion est validee
	 * 
	 * @param enseignant l'enseignant a ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Enseignant enseignant) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("INSERT INTO Enseignant(idEnseignant, nom_en, prenom_en, password, email_en, numtel, bureau) VALUES(?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, enseignant.getId());
			ps.setString(2, enseignant.getNom());
			ps.setString(3, enseignant.getPrenom());
			ps.setString(4, enseignant.getPassword());
			ps.setString(5, enseignant.getEmail());
			ps.setInt(6, enseignant.getNumtel());
			ps.setString(7, enseignant.getBureau());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de enseignant existe déjà. Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de modifier un fournisseur dans la table supplier.
	 * Le mode est auto-commit par defaut : chaque modification est validee
	 * 
	 * @param supplier le fournisseur a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	public int update(Enseignant enseignant) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("UPDATE Enseignant set nom_en = ?, prenom_en = ?, password = ?, email_en = ?, numtel = ?, bureau = ? WHERE idEnseignant = ?");
			ps.setString(1, enseignant.getNom());
			ps.setString(2, enseignant.getPrenom());
			ps.setString(3, enseignant.getPassword());
			ps.setString(4, enseignant.getEmail());
			ps.setInt(5, enseignant.getNumtel());
			ps.setString(6, enseignant.getBureau());
			ps.setInt(7, enseignant.getId());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de supprimer un enseignant par id dans la table enseignant.
	 * Si ce dernier possede des articles, la suppression n'a pas lieu.
	 * Le mode est auto-commit par defaut : chaque suppression est validee
	 * 
	 * @param id l'id de enseignant à supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int delete(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID du fournisseur
			ps = con.prepareStatement("DELETE FROM Enseignant WHERE idEnseignant = ?");
			ps.setInt(1, id);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-02292"))
				System.out.println("Ce fournisseur possede des articles, suppression impossible !"
						         + " Supprimer d'abord ses articles ou utiiser la méthode de suppression avec articles.");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	/**
	 * Permet de recuperer un enseignant a partir de sa reference
	 * 
	 * @param reference la reference de enseigannt a recuperer
	 * @return le fournisseur trouve;
	 * 			null si aucun fournisseur ne correspond a cette reference
	 */
	public Enseignant get(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Enseignant returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Enseignant WHERE idEnseignant = ?");
			ps.setInt(1, id);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Enseignant(rs.getInt("idEnseignant"),
									       rs.getString("nom_en"),
									       rs.getString("prenom_en"),
									       rs.getString("password"),
									       rs.getString("email_en"),
									       rs.getInt("numtel"),
									       rs.getString("bureau"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	public ResultSet getAll() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Enseignant");

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
		}
		catch (Exception ee) {
				ee.printStackTrace();
		
		
	}
		return rs;
}

	}