/**
 * 
 */
package DAO;

import java.sql.*;
import java.util.ArrayList;
import model.Enseignant ;
import net.proteanit.sql.DbUtils;

import java.awt.EventQueue;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 *  Classe permettant de gerer les absences
 * @author Kevin ADJIWANOU
 *
 *
 */
public class AbsenceDAO extends ConnectionDAO {
	
	/*
	 * Constructor de AbsenceDAO
	 */
	public AbsenceDAO() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 * @param groupe Pour le numero du groupe
	 * @param table Pour pouvoir afficher la liste de planning
	 * 
	 */
	public void idp(int groupe, JTable table){
		  
		  Connection con = null;
		  PreparedStatement ps = null;
		  ResultSet rs= null; 
		  
		  // connexion a la base de donnees
		  try {
		  // tentative de connexion
		  con = DriverManager.getConnection(URL, LOGIN, PASS);
		  // preparation de l'instruction SQL, le ? represente la valeur de l'ID
		  // a communiquer dans la suppression.
		  // le getter permet de recuperer la valeur de l'ID du fournisseur
		  ps = con.prepareStatement("SELECT duree, matiere, intervenant FROM planning INNER JOIN  planningpargroupe ON (idplanningpargroupe=idplanning) WHERE idgroupeparplanning = ? AND dates = TRUNC(SYSDATE) ORDER BY duree");
		  ps.setInt(1, groupe);
		  rs = ps.executeQuery();
		  // Execution de la requete
		  table.setModel(DbUtils.resultSetToTableModel(rs));
		  con.close();
	    
		  } catch (Exception e) {
		  e.printStackTrace(); 
		  } 
		 
	}
	

	/*
	 * 
	 * @param absence Pour stocker absence
	 * @param table Pour afficher les absences
	 */
	public void abs(String absence,JTable table){
		  
		  Connection con = null;
		  PreparedStatement ps = null;
		  ResultSet rs= null; 
		  
		  // connexion a la base de donnees
		  try {
		  // tentative de connexion
		  con = DriverManager.getConnection(URL, LOGIN, PASS);
		  // preparation de l'instruction SQL, le ? represente la valeur de l'ID
		  // a communiquer dans la suppression.
		  // le getter permet de recuperer la valeur de l'ID du fournisseur
		  ps = con.prepareStatement("SELECT * FROM absence INNER JOIN  heureabsence ON (idabs=idabsence) WHERE idetu = ? AND dates = TRUNC(SYSDATE)");
		  ps.setString(1, absence);
		  rs = ps.executeQuery();
		  // Execution de la requete
		  table.setModel(DbUtils.resultSetToTableModel(rs));
		  con.close();
	    
		  } catch (Exception e) {
		  e.printStackTrace(); 
		  } 
}
}




