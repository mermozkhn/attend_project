/**
 * 
 */
package DAO;
import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Enseignant;
import model.Gestionnaire ;
import GUI.AuthentificationGUI;
/**
 *  La classe permettant de faire la vérification et l'authentification 
 * @author Mermoz KANHONOU
 *@version 1.0
 * 
 */
public class AuthentificationDAO extends ConnectionDAO {
	public int Verification(String ide, String motpasse) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Gestionnaire returnValue = null;
		int value = 0;
		
		// connexion a la base de donnees
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM gestionnaire WHERE idg = 1");
					// on execute la requete
					// rs contient un pointeur situe juste avant la premiere ligne retournee
					rs = ps.executeQuery();
					// passe a la premiere (et unique) ligne retournee
					if (rs.next()) {
						returnValue = new Gestionnaire(rs.getInt("idg"),
											       rs.getString("identifiant"),
											       rs.getString("pasword"));
						if(returnValue.getIdentifiant().equals(ide) && returnValue.getPasword().equals(motpasse)) {
							value = 1;
						}
						else {
							value = 0;
							JOptionPane.showMessageDialog(null, " Erreur identifiant ou mot de passe incorrecte");
						}
					}
				} catch (Exception ee) {
					ee.printStackTrace();
				} finally {
					// fermeture du ResultSet, du PreparedStatement et de la Connexion
					try {
						if (rs != null) {
							rs.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (con != null) {
							con.close();
						}
					} catch (Exception ignore) {
					}
				}
				return value;
			}

}
