package DAO;
import model.Cours;
import java.sql.*;
import java.util.ArrayList;

/**
 * Classe permettant de gérer tout ce qui a rapport avec la database de Cours
 * @author Mermoz KANHONOU
 * @version 1.0
 * 
 *
 */
public class CoursDAO extends ConnectionDAO {

	/**
	 * 
	 */
	public CoursDAO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

			/**
			 * Permet d'ajouter un Cours dans la table cours.
			 * Le mode est auto-commit par defaut : chaque insertion est validee
			 * 
			 * @param Cours le cours a ajouter
			 * @return retourne le nombre de lignes ajoutees dans la table
			 */
			public int add(Cours Cours) {
				Connection con = null;
				PreparedStatement ps = null;
				int returnValue = 0;

				// connexion a la base de donnees
				try {

					// tentative de connexion
					con = DriverManager.getConnection(URL, LOGIN, PASS);
					// preparation de l'instruction SQL, chaque ? represente une valeur
					// a communiquer dans l'insertion.
					// les getters permettent de recuperer les valeurs des attributs souhaites
					ps = con.prepareStatement("INSERT INTO Cours(idCours, nom, masseHoraire, departement) VALUES(?, ?, ?, ?)");
					ps.setInt(1, Cours.getIdCours());
					ps.setString(2, Cours.getNom());
					ps.setDouble(3, Cours.getMasseHoraire());
					ps.setString(4, Cours.getDepartement());

					// Execution de la requete
					returnValue = ps.executeUpdate();

				} catch (Exception e) {
					if (e.getMessage().contains("ORA-00001"))
						System.out.println("Cet identifiant de cours existe déjà. Ajout impossible !");
					else
						e.printStackTrace();
				} finally {
					// fermeture du preparedStatement et de la connexion
					try {
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (con != null) {
							con.close();
						}
					} catch (Exception ignore) {
					}
				}
				return returnValue;
			}

			/**
			 * Permet de modifier un cours dans la table cours.
			 * Le mode est auto-commit par defaut : chaque modification est validee
			 * 
			 * @param cours le cours a modifier
			 * @return retourne le nombre de lignes modifiees dans la table
			 */
			public int update(Cours enseignant) {
				Connection con = null;
				PreparedStatement ps = null;
				int returnValue = 0;

				// connexion a la base de donnees
				try {

					// tentative de connexion
					con = DriverManager.getConnection(URL, LOGIN, PASS);
					// preparation de l'instruction SQL, chaque ? represente une valeur
					// a communiquer dans la modification.
					// les getters permettent de recuperer les valeurs des attributs souhaites
					ps = con.prepareStatement("UPDATE Cours set nom = ?, masseHoraire = ?, departement = ? WHERE idCours = ?");
					ps.setString(1, enseignant.getNom());
					ps.setDouble(2, enseignant.getMasseHoraire());
					ps.setString(3, enseignant.getDepartement());
					ps.setInt(4, enseignant.getIdCours());

					// Execution de la requete
					returnValue = ps.executeUpdate();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// fermeture du preparedStatement et de la connexion
					try {
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (con != null) {
							con.close();
						}
					} catch (Exception ignore) {
					}
				}
				return returnValue;
			}

			/**
			 * Permet de supprimer un cours par id dans la table cours.
			 *
			 * Le mode est auto-commit par defaut : chaque suppression est validee
			 * 
			 * @param id l'id du cours à supprimer
			 * @return retourne le nombre de lignes supprimees dans la table
			 */
			public int delete(int id) {
				Connection con = null;
				PreparedStatement ps = null;
				int returnValue = 0;

				// connexion a la base de donnees
				try {

					// tentative de connexion
					con = DriverManager.getConnection(URL, LOGIN, PASS);
					// preparation de l'instruction SQL, le ? represente la valeur de l'ID
					// a communiquer dans la suppression.
					// le getter permet de recuperer la valeur de l'ID du etudiant
					ps = con.prepareStatement("DELETE  FROM Cours WHERE idCours = ?");
					ps.setInt(1, id);
					// Execution de la requete
					returnValue = ps.executeUpdate();

				} catch (Exception e) {
					if (e.getMessage().contains("ORA-02292"))
						System.out.println("Ce fournisseur possede rien de particylier, suppression impossible !"
								         + " Supprimer d'abord ses articles ou utiiser la méthode de suppression avec articles.");
					else
						e.printStackTrace();
				} finally {
					// fermeture du preparedStatement et de la connexion
					try {
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (con != null) {
							con.close();
						}
					} catch (Exception ignore) {
					}
				}
				return returnValue;
			}


			/**
			 * Permet de recuperer un cours a partir de sa reference
			 * 
			 * @param reference la reference de cours a recuperer
			 * @return le cours trouve;
			 * 			null si aucun cours ne correspond a cette reference
			 */
			public Cours get(int id) {
				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				Cours returnValue = null;

				// connexion a la base de donnees
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM Cours WHERE idCours = ?");
					ps.setInt(1, id);

					// on execute la requete
					// rs contient un pointeur situe juste avant la premiere ligne retournee
					rs = ps.executeQuery();
					// passe a la premiere (et unique) ligne retournee
					if (rs.next()) {
						returnValue = new Cours(rs.getInt("idCours"),
											       rs.getString("nom"),
											       rs.getDouble("masseHoraire"),
											       rs.getString("departement")
											       );
					}
				} catch (Exception ee) {
					ee.printStackTrace();
				} finally {
					// fermeture du ResultSet, du PreparedStatement et de la Connexion
					try {
						if (rs != null) {
							rs.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (ps != null) {
							ps.close();
						}
					} catch (Exception ignore) {
					}
					try {
						if (con != null) {
							con.close();
						}
					} catch (Exception ignore) {
					}
				}
				return returnValue;
			}

		
			public ResultSet getAll() {
				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM Cours");

					// on execute la requete
					// rs contient un pointeur situe juste avant la premiere ligne retournee
					rs = ps.executeQuery();
				}
				catch (Exception ee) {
						ee.printStackTrace();
				
				
			}
				return rs;
		}
			public ResultSet countAll() {
				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT COUNT (*) FROM Cours");

					// on execute la requete
					// rs contient un pointeur situe juste avant la premiere ligne retournee
					rs = ps.executeQuery();
				}
				catch (Exception ee) {
						ee.printStackTrace();
				
				
			}
				return rs;
		}
	}
	// ORA-01438 : pour une erreur à une case pour pas avoir remplir corrcetement la case 


